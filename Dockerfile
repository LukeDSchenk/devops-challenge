# Initially, I wanted to use Alpine Linux for its small footprint (both in terms of bytes and security)
# Unfortunately on Alpine linux, it appears there are certain unmet dependencies for running litecoin
# I like to specify an explicit image version rather than "latest" to prevent deployments from breaking
FROM debian:stable-20211011-slim

# Install wget for retrieving the litecoin binary
RUN apt-get update
RUN apt-get install -y wget

# Initialize a working directory and a non-root user with proper permissions on said dir
WORKDIR /opt/litecoin
RUN mkdir data
RUN chown -R 1099:1099 /opt/litecoin
USER 1099

# The link to the official linux release binary for litecoin 0.18.1
# and the Sha256 checksum of the binary, as stated here: https://github.com/litecoin-project/litecoin/releases/tag/v0.18.1
ARG RELEASE_URL="https://download.litecoin.org/litecoin-0.18.1/linux/litecoin-0.18.1-x86_64-linux-gnu.tar.gz"
ARG FILE_NAME="litecoin-0.18.1-x86_64-linux-gnu.tar.gz"
ARG SHA256SUM="ca50936299e2c5a66b954c266dcaaeef9e91b2f5307069b9894048acf3eb5751"

# Download the release file and verify the checksum (the build will fail if it is invalid)
# I found the way I verified the checksum to be a bit clunky, so instead I followed this great example of piping to the '-c' flag of sha256sum: https://stackoverflow.com/a/47282426
RUN wget $RELEASE_URL
RUN echo "Verifying checksum..."
RUN echo "$SHA256SUM  $FILE_NAME" | sha256sum -c
RUN echo "Checksum is valid"

# Extract the data from the archive file
RUN tar xvzf $FILE_NAME

# Use the litecoin daemon as the default entry command for the container, and specify our data directory (this helps when running as a non-root user on kubernetes with persistent volumes)
CMD /opt/litecoin/litecoin-0.18.1/bin/litecoind --datadir=/opt/litecoin/data
