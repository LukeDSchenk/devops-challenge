# DevOps Challenge

A few DevOps challenges.

## Table of Contents

1. **Docker-ayes:** See `Dockerfile`.
2. **k8s-FTW:** See `litecoin-statefulset.yaml`.
3. **All the continuouses:** See `.gitlab-ci.yml` and `./gitlab-pipeline-results/*`. 
4. **Script kiddies:** See `./text-scripting/README.md` and `./text-scripting/create-list.sh`.
5. **Script grown-ups:** See `./text-scripting/README.md` and `./text-scripting/create-list.py`.
6. **Terraform lovers unite:** I have chosen to skip this one for now, due to my lack of experience with Terraform, and lack of time as I am currently in the process of moving. I would like to dive into TF and give this one a go in the near future. 

Thanks, and please feel free to share any feedback with me at *L.Schenk21@gmail.com*.
