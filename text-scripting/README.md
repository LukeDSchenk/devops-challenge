# The Problem

Here is a silly problem I came up with myself:

**You are a database admin for a cool website which sells luxury screws, and currently has 300 users. Google has recently announced that they are changing GMail accounts for all of their users from** *@gmail.com* **addresses, to** *@spymail.net*. **You must update the email addresses of these users in your database accordingly.**

**At the same time, the entire country of Canada has been taken over by the newly formed country of Flemington! Therefore, all email addresses with a** *.ca* **extension must now be replaced with the extension of this new country:** *.flem*.

**As if the news about Canada was not tragic enough, the letter** *P* **has also been replaced in the English dictionary by the symbol:** *$* **(as an honorary gesture for Pluto which has been deemed not a planet many years ago). All upper/lowercase instances of the letter** *P* **must be replaced immediately.**

**One of your developers informs you that they have written a script to automate all of the email updates for you. They tell you that the script works by taking in a list of addresses in this format:**

```bash
oldaddress@oldmail.com:newaddress@newmail.org
someguy@gmail.com:someguy@spymail.net
applesauce@yahoo.ca:a$$lesauce@yahoo.flem
```

**Write a script/program which can take your list of user emails (`random-emails.txt`) and create a new list containing the proper format for updating the database. The new list should only contain entries that need to be updated in the database!**

*All emails used in this problem were randomly generated.*

# The Solution

I have created solutions to this problem using a combination of sed/grep, as well as Python. I have checked that both versions will produce the exact same text output (so I really hope I didn't miss anything in them :)).

To run these solutions, simply use:

`./create-list.sh`

or

`python3 create-list.py`

I hope you have enjoyed my silly problem!
