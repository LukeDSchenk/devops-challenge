# Create a curated list of emails only containing addresses which need to be updated.
# We are basically just grepping for the three different patterns that require a change.
# The new list is written to its own file.
cat random-emails.txt | grep -i -e "\.ca" -e "gmail\.com" -e "p" > update-list.txt

# Next, we loop over each line in the list of addresses to be updated, and use
# sed to implement the necessary substitutions.
# In this case, we want to substitute "P" last, because we want the new 'spymail.net'
# addresses to be 's$ymail.net'.
while read line; do
    new=`echo "$line" | sed 's/\.ca/\.flem/g; s/gmail\.com/spymail\.net/g; s/p/$/g;'`
    echo $line:$new
done < update-list.txt

# This is run just to clean up the intermediate file. 
rm update-list.txt
