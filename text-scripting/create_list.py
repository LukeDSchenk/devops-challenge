#An empty list, to hold the emails as we update them.
updated_emails = []

# Open the list of emails with read permissions, and read its lines into a list.
# I used a context manager here so I don't have to worry about forgetting to
# close the file.
with open("random-emails.txt", "r") as fp:
    emails = fp.readlines()

# Loop through each email in the list.
for addr in emails:
    new_addr = addr

    # Here is where we apply the substitutions.
    # Once again, P is substituted last for the spymail.net addresses to be
    # updated properly.
    if '.ca' in addr:
        new_addr = new_addr.replace('.ca', '.flem')
    elif 'gmail.com' in addr:
        new_addr = new_addr.replace('gmail.com', 'spymail.net')

    if 'P' in new_addr or 'p' in new_addr:
        new_addr = new_addr.replace('p', '$')
        new_addr = new_addr.replace('P', '$')

    # If there have been changes made to an address, then we add it to the list
    # of updated emails (and format it simultaneously).
    if new_addr != addr:
        updated_emails.append(f"{addr.strip()}:{new_addr.strip()}")

# Loop through the updated email list and print each entry.
for email in updated_emails:
    print(email)
